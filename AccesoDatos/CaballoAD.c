//Contiene los datos que se recogen de los caballos

//Funciones de biblioteca
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <stdbool.h>
#include <strings.h>
#include "../InterfazUsuario/caballoIU.h"
#include "../InterfazSistema/CaballoSys.h"
#include "CaballoAD.h"
#include "../InterfazUsuario/interfazGrafica.h"


int cargaListaCaballosAD(char nombre[][30],long DIE[],int victorias[],float ganancias[])//Carga la lista de caballos registrados en los vectores paralelos que recibe como par�metros.
{
    int i=0;//Valor que se da a la variable int para que se inicie correctamente
    FILE* archivo2=fopen("BaseDatos/caballos.txt","rt");//Abre el archivo caballos.txt
    if(archivo2==NULL)//Si el archivo2 est� vac�o, se ejecuta lo que se encuentra a continuaci�n
    {
        gotoxy(6,20);
        printf("ERROR. No se puede abrir el archivo.\n");
        return (-1);//El programa finaliza
    }
    while(fscanf(archivo2,"%ld\t%d\t%f\t",&DIE[i],&victorias[i],&ganancias[i])==3)//Se ejecuta el bucle while, si se leen 4 datos en la posici�n 'i', el contador aumenta
        //y sigue leyendo hasta que no haya m�s caballos en caballos.txt
    {
        fgets(nombre[i],sizeof(nombre[i]),archivo2);
        i++;//El contador aumenta
    }
    fclose(archivo2);//Se cierra el archivo2
    return(i);//Devuelve la posici�n 'i'
}

bool guardaListaCaballosAD(int x,char nombre[][30], long DIE[],int victorias[],float ganancias[])
{
    int i;
    FILE* archivo2=fopen("BaseDatos/caballos.txt","wt");

    if (archivo2==NULL)
    {
        return false;
    }
    else
    {
        for(i=0;i<x-1;i++)
        {
            escribeCaballoTXT(nombre[i],DIE[i],victorias[i],ganancias[i],archivo2);
        }
    }

    return true;
}

void escribeCaballoTXT(char nombre[],long DIE,int victorias,float ganancias,FILE* archivo1)//Agrega los datos de un caballo al archivo de informe de caballos.
{
    fprintf(archivo1,"%ld\t%d\t%8.2f\t%s\n",DIE,victorias,ganancias,nombre);//En caso contario, se ejecuta esta instrucci�n y se muestra en infoCaballos.txt los datos del caballo
}

int altaCaballoAD(char nombre[],long DIE, int victorias, float ganancias)//Agrega los datos del nuevo caballo al archivo de texto que contiene los caballos registrados.
{
    FILE* archivo2=fopen("BaseDatos/caballos.txt","at+");//Abrir archivo en modo "add text", a�adiendo datos desde la �ltima posici�n en la que se encuentra el cursor

    if (archivo2==NULL)//Si el archivo2 est� vac�o, se ejecuta lo que se encuentra a continuaci�n
    {
        return -1;//El programa finaliza
    }

    fprintf(archivo2, "%ld\t%d\t%8.0f\t%s\n", DIE, victorias, ganancias, nombre);//En caso contario, se ejecuta esta instrucci�n y se muestra en caballos.txt los datos del caballo
    fclose(archivo2);//Se cierra el archivo
    return 0;
}

int guardaListaCaballosDAT(int x, char nombre[][30],long DIE[],int victorias[],float ganancias[])
{
    int i;//Variable de tipo int para el contador
    i=0;

    FILE* archivo2=fopen("BaseDatos/caballos.txt","wt");

    if (archivo2==NULL)//Si el archivo2 est� vac�o, se ejecuta la siguiente instrucci�n
    {
        return -1;//El programa finaliza
    }

    else
    {
        for (i=0; i<x; i++)
        {
            escribeCaballoTXT(nombre[i],DIE[i],victorias[i],ganancias[i],archivo2);
        }
    }
    fclose(archivo2);

    return 0;
}

