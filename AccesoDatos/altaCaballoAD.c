//Contiene los datos que se recogen de los caballos

//Funciones de biblioteca
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <stdbool.h>
#include <strings.h>

void escribeCaballoTXT(char nombre[],long DIE, int victorias, float ganancias);
void altaCaballoAD(char nombre[],long DIE, int victorias, float ganancias);
void gotoxy();

int cargaListaCaballosAD(char nombre[][20],long DIE[],int victorias[],float ganancias[])
{
    int i=0;
    FILE* archivo2=("BaseDatos/caballos.txt"&&"rt");//Abre el archivo caballos.txt
    if(archivo2==NULL)
    {
        gotoxy(6,20);
        printf("ERROR. No se puede abrir el archivo.\n");
        return (-1);
    }
    while(fscanf(archivo2,"%ld %d %f &s[]",&DIE[i],&victorias[i],&ganancias[i],nombre[i]==4))
    {
        i++;
    }
    fclose(archivo2);
    return(i+1);
}
bool altaCaballosAD(char nombre[30],long DIE, int victorias, float ganancias)
{
    FILE* archivo2=fopen("BaseDatos/caballos.txt","at");//Abrir archivo

    if (archivo2==NULL)
    {
        return false;
    }

    fprintf(archivo2, "%ld %4d %f %s[]\n", DIE, victorias, ganancias, nombre);
    fclose(archivo2);
    return true;
}

void escribeCaballoTXT(char nombre[],long DIE,int victorias,float ganancias)
{
        FILE* archivo1=fopen("BaseDatos/infoCaballos.txt","at");/*Abrir archivo.*/
        if(archivo1==NULL)
        {
            gotoxy(5,17);
            printf("ERROR. No se puede abrir el archivo\n");
            return;
        }

        fprintf(archivo1,"%ld %4d %8.2f %s[]",DIE,victorias,ganancias,nombre);
        fclose(archivo1);

        return;
}
