#ifndef CABALLOAD_H_INCLUDED
#define CABALLOAD_H_INCLUDED

void escribeCaballoTXT(char nombre[],long DIE, int victorias, float ganancias,FILE* archivo1);//Funci�n que escribe en infoCaballos.txt
int altaCaballoAD(char nombre[],long DIE, int victorias, float ganancias);//Funci�n que muestra en caballos.txt los datos de los caballos guardados
int cargaListaCaballosAD(char nombre[][30], long die[], int victorias[], float ganancias[]);//Se llama a esta funci�n para recoger los datos de los caballos guardados
int guardaListaCaballosDAT(int x,char nombre[][30],long DIE[],int victorias[],float ganancias[]);//Esta funci�n recibe como datos el n�mero de caballos a guardar
bool guardaListaCaballosAD(int x,char nombre[][30], long DIE[],int victorias[],float ganancias[]);

#endif // CABALLOAD_H_INCLUDED
