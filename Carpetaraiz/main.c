//PROGRAMACI�N: PROYECTO HIP�DROMO.
//Autor: Ver�nica Lech�n Rodr�guez. DNI: 71964282L

//Funciones de biblioteca
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

//Prototipo de funciones
void gotoxy (int x, int y); //Sit�a el cursor en un sitio deseado de la pantalla
void setColorTexto (WORD colors); //Permite cambiar de color los caracteres impresos en pantalla
void inicInterfazUsuario();//Determina el tama�o y el t�tulo de la consola, adem�s dibuja las l�neas de la interfaz gr�fica
int menuPrincipal ();//Muestra las opciones generales del proyecto Hip�dromo
void gestionMenuPrincipal ();//Gestiona las opciones del men� principal

int main () //Funci�n principal
{
    //Declaraci�n de variables
    long int DIE; //N�mero de identificaci�n equino
    char i; //Variable de iteraci�n en los bucles 'for' de l�neas horizontales y verticales
    int victorias; //N�mero de victorias de los caballos
    float ganancias; //Cantidad de ganancias obtenidas

    //CMD
    system("Title Proyecto Integrador"); //T�tulo del programa al ejecutarlo
    system("mode con cols=125 lines=35"); //�rea del programa
    setColorTexto(7); //El texto se imprimir� en pantalla seg�n el c�digo de color escogido


    //INTERFAZ


        //RECUADRO SUPERIOR

        gotoxy(0,124);//Esquina arriba izda.
        printf("%c",201);

        gotoxy(1,0);//L�neas dobles horizontales arriba
          for (i=2;i<=124;i++)
            {
                printf("%C",205);
            }

        gotoxy(0,1);//L�nea vertical doble izda.
        printf("%c",186);

        gotoxy(53,1);//T�tulo
        printf("H I P \242 D R O M O");

        gotoxy(0,2);//Esquina doble abajo izda.
        printf("%c",200);

        gotoxy(124,0);//Esquina doble arriba dcha.
        printf("%c",187);

        gotoxy(1,2);//L�neas dobles horizontales abajo
            for (i=2;i<=124;i++)
            {
                printf("%C",205);
            }

        gotoxy(124,1);//L�nea vertical doble dcha.
        printf("%c",186);

        gotoxy(124,2);//Esquina doble abajo dcha.
        printf("%c",188);




        //RECUADRO CENTRAL IZDO.

        gotoxy(0,3);//Esquina superior izda.
        printf("%c",218);

        gotoxy(1,3);//L�nea horizontal simple inferior
        for (i=1;i<=60;i++)
            {
                printf("%C",196);
            }

        gotoxy(61,3);//Esquina superior derecha
        printf("%c",191);

        gotoxy(61,4);//L�nea vertical
        printf("%c",179);

        gotoxy(0,4);//L�nea vertical
        printf("%c",179);

        gotoxy(0,5);//Esquina 3 ramas hacia la dcha.
        printf("%c",195);

        gotoxy(0,6);//L�nea vertical
        printf("%c",179);

        gotoxy(1,5);//L�nea horizontal simple superior
            for (i=1;i<=61;i++)
            {
                printf("%C",196);
            }

        gotoxy(1,7);//L�nea horizontal simple inferior
        for (i=1;i<=61;i++)
            {
                printf("%C",196);
            }

        gotoxy(1,6);//L�nea vertical simple izda.
            for (i=1;i<=12;i++)
            {
                gotoxy(0,i+7);
                printf("%c",179);
            }

        gotoxy(6,4);//Men� principal
        printf("           M e n u  p r i n c i p a l");


        gotoxy(61,5);//Esquina arriba dcha.
        printf("%c",180);

        gotoxy(61,6);//L�nea vertical dcha.
        printf("%c",179);

         gotoxy(61,7);//L�nea vertical simple dcha.
            for (i=1;i<=12;i++)
            {
                gotoxy(61,i+7);
                printf("%c",179);
            }

        gotoxy(61,7);//Esquina 3 ramas dcha.
        printf("%c",180);

        gotoxy(0,7);//Esquina 3 ramas izda.
        printf("%c",195);

        gotoxy(0,20);//Esquina abajo izda.
        printf("%c",192);

        gotoxy(0,7);//L�nea vertical simple izda.

        gotoxy(1,20);//L�nea simple abajo
            for (i=1;i<=60;i++)
            {
                printf("%c",196);
            }

        gotoxy(61,20);//Esquina abajo dcha.
        printf("%c",217);




        //RECUADRO DCHO.

        gotoxy(62,3);//Esquina superior izda.
        printf("%c",218);

        gotoxy(63,3);//L�nea horizontal simple inferior
        for (i=1;i<=61;i++)
            {
                printf("%C",196);
            }

        gotoxy(125,3);//Esquina superior derecha
        printf("%c",191);

        gotoxy(124,4);//Esquina superior derecha
        printf("%c",179);

        gotoxy(124,5);//Esquina 3 ramas hacia la izda.
        printf("%c",180);

        gotoxy(124,6);//L�nea vertical
        printf("%c",179);

        gotoxy(62,4);//L�nea vertical
        printf("%c",179);

        gotoxy(62,4);//L�nea vertical
        printf("%c",179);

        gotoxy(62,5);//Esquina 3 ramas hacia la dcha.
        printf("%c",195);

        gotoxy(62,6);//L�nea vertical
        printf("%c",179);

        gotoxy(63,5);//L�nea horizontal simple superior
            for (i=1;i<=61;i++)
            {
                printf("%C",196);
            }

        gotoxy(63,7);//L�nea horizontal simple inferior
        for (i=1;i<=61;i++)
            {
                printf("%c",196);
            }


        gotoxy(125,5);//Esquina 3 ramas dcha.
        printf("%c",180);

        gotoxy(125,6);//L�nea vertical dcha.
        printf("%c",179);

        gotoxy(124,7);//L�nea vertical simple dcha.
            for (i=1;i<=12;i++)
            {
                gotoxy(124,i+7);
                printf("%c",179);
            }

        gotoxy(61,7);//Esquina 3 ramas dcha.
        printf("%c",180);

        gotoxy(62,7);//Esquina 3 ramas izda.
        printf("%c",195);

        gotoxy(0,20);//Esquina abajo izda.
        printf("%c",192);

        gotoxy(62,8);//L�nea vertical izda.
            for(i=1;i<=12;i++)
            {
               gotoxy(62,i+7);
               printf("%c",179);
            }

        gotoxy(62,20);//Esquina abajo izda.
        printf("%c",192);

        gotoxy(63,20);//L�nea simple abajo
            for (i=1;i<=61;i++)
            {
                printf("%c",196);
            }

        gotoxy(124,20);//Esquina abajo dcha.
        printf("%c",217);



    //RECUADRO INFERIOR

        gotoxy(4,22);//L�nea superior horizontal
        for (i=5;i<=121;i++)
        {
            printf("%c",196);
        }

        gotoxy(4,25);//L�nea inferior horizontal
        for (i=5;i<=121;i++)
        {
            printf("%c",196);
        }

        gotoxy(3,22);//Esquina superior izda.
        printf("%c",218);

        gotoxy(3,25);//Esquina inferior izda.
        printf("%c",192);

        gotoxy(3,23);//L�nea vertical izda.
        printf("%c",179);

        gotoxy(3,24);//L�nea vertical izda.
        printf("%c",179);

        gotoxy(121,22);//Esquina superior dcha.
        printf("%c",191);

        gotoxy(121,25);//Esquina inferior dcha.
        printf("%c",217);

        gotoxy(121,23);//L�nea vertical dcha
        printf("%c",179);

        gotoxy(121,24);//L�nea vertical dcha.
        printf("%c",179);



        //ENTRADA Y SALIDA DE DATOS
        //Entrada y salida de datos

        gotoxy(6,23);
        printf("DIE:\n");

        gotoxy(6,24);
        scanf("\n%ld",&DIE);//Se guarda el valor en la variable DIE

        fflush(stdin);

        gotoxy(6,23);
        printf("Ganancias:\n");

        gotoxy(6,24);
        printf("          ");//Espacio para que no aparezca lo escrito anteriormente

        gotoxy(6,24);
        scanf("\n%f",&ganancias);//Se guarda el valor introducido en la variable ganancias

        fflush(stdin);

        gotoxy(6,23);
        printf("Victorias:\n");

        gotoxy(6,24);
        printf("          ");

        gotoxy(6,24);
        scanf("\n%i",&victorias);//Se guarda el valor introducido en la variable victorias

    return 0;

}

void gotoxy (int x, int y) //Funci�n declarada de la situaci�n del cursor en la pantalla
{
    COORD coord;

    coord.X = x;
    coord.Y = y;

    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);

    return;
}

void setColorTexto (WORD colors) //Funci�n declarada de los colores usados en la interfaz
{
    HANDLE hConsole=GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, colors);
    return;
}
