//PROGRAMACI�N: PROYECTO HIP�DROMO.
//Autor: Ver�nica Lech�n Rodr�guez. DNI: 71964282L

//Funciones de biblioteca
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <locale.h>
#include <conio.h>
#include <stdbool.h>
#include "InterfazUsuario/interfazGrafica.h"
#include "InterfazUsuario/interfazUsuario.h"


//Prototipo de funciones
void gotoxy (int x, int y); //Sit�a el cursor en un sitio deseado de la pantalla
void setColorTexto (WORD colors); //Permite cambiar de color los caracteres impresos en pantalla
void inicInterfazUsuario();//Determina el tama�o y el t�tulo de la consola, adem�s dibuja las l�neas de la interfaz gr�fica
int menuPrincipal ();//Muestra las opciones generales del proyecto Hip�dromo
void gestionMenuPrincipal ();//Gestiona las opciones del men� principal
char* setlocale(int categoria, const char*local);



int main () //Funci�n principal
{
    //CMD
    system("color 3B");
    system("Title Proyecto Integrador"); //T�tulo del programa al ejecutarlo
    system("mode con cols=125 lines=30"); //�rea del programa
    setColorTexto(63); //El texto se imprimir� en pantalla seg�n el c�digo de color escogido

    inicInterfazUsuario();
    gestionMenuPrincipal();


    gotoxy(0,30);
    return 0;
}


