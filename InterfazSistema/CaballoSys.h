#ifndef CABALLOSYS_H_INCLUDED
#define CABALLOSYS_H_INCLUDED

bool altaCaballoSys(char nombre[],long DIE, int victorias, float ganancias);//Se env�an los datos recogidos en la funci�n altaCaballoIU a altaCaballoSys
bool generaInformeCaballos();//Genera el informe de caballos
int cargaListaCaballosSys(char nombre[][30],long DIE [],int victorias[],float ganancias[],char msg[]);//Se llama a esta funci�n para recoger los datos de los caballos guardados
bool guardaListaCaballosSys(int x,char nombre[][30],long DIE[],int victorias[],float ganancias[],char msg[]);


#endif // CABALLOSYS_H_INCLUDED
