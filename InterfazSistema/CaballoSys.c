//Contiene las funciones del sistema relacionadas con los caballos

//Funciones de biblioteca
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <stdbool.h>
#include "../InterfazSistema/CaballoSys.h"
#include "../AccesoDatos/CaballoAD.h"
#include "../InterfazUsuario/interfazGrafica.h"


bool generaInformeCaballos()//Genera un archivo de texto con los datos de TODOS los caballos.
{

    long DIE;
    int victorias;
    float ganancias;
    char nombre [20];

    FILE* archivo2 =fopen("BaseDatos/caballos.txt","rt");//Se abre el archivo en modo lectura
    FILE* archivo1 =fopen("BaseDatos/infoCaballos.txt","wt");//Se abre el archivo en modo escritura

    if (archivo1==NULL||archivo2==NULL)//Se comprueba que ambos archivos se abren correctamente
    {
        gotoxy(10,15);
        printf("ERROR. No se puede abrir el archivo.");
        return false;
    }

    fprintf(archivo1,"-------------------------------------------------\n");
    fprintf(archivo1,"DIE      Victorias    Ganancias    Nombre\t\n");
    fprintf(archivo1,"-------------------------------------------------\n");

    while(fscanf(archivo2,"%ld\t%d\t%f\t",&DIE,&victorias,&ganancias)==3)//Se ejecuta el bucle while, si se leen 4 datos en la posici�n 'i', el contador aumenta
        //y se env�an los datos a la funci�n escribeCaballoTXT
    {
        fgets(nombre, sizeof(nombre),archivo2);
        escribeCaballoTXT(nombre,DIE,victorias,ganancias,archivo1);//Llamada a la funci�n escribecaballoTXT
    }

    fclose(archivo1);//Se cierra infoCaballos.txt
    fclose(archivo2);//Se cierra caballos.txt
    system("start notepad BaseDatos/infoCaballos.txt");//Abre el archivo infoCaballos.txt
    muestraMensajeInfo("Informe correctamente generado. Pulse <enter> para continuar.");//Muestra un mensaje por pantalla
    getch();//El programa espera a que el usuario introduzca un caracter por pantalla para pasar a la siguiente instrucci�n
    muestraMensajeInfo("                                                              ");

    return true;
}


bool altaCaballoSys(char nombre[],long DIE,int victorias,float ganancias)//Transmite los datos del nuevo caballo desde la interfaz de usuario a la capa de acceso a datos.
{
    return altaCaballoAD(nombre,DIE,victorias,ganancias);//Se env�an los datos a la funci�n altaCaballoAD
}


int cargaListaCaballosSys(char nombre[][30],long DIE[],int victorias[],float ganancias[],char msg[])//Transmite la orden de cargar los datos de los caballos.
{
    if(cargaListaCaballosAD(nombre,DIE,victorias,ganancias)==-1)//Si no funciona correctamente el programa se cierra
    {
        return (-1);//Devuelve el valor '-1' que hace que el programa se finalice
    }
    else
    {
        return cargaListaCaballosAD(nombre,DIE,victorias,ganancias);//Si funciona correctamente los datos son enviados a la funci�n cargaListaCaballosAD
    }
}

bool guardaListaCaballosSys(int x,char nombre[][30],long DIE[],int victorias[],float ganancias[],char msg[])
{
    if (guardaListaCaballosDAT(x,nombre,DIE,victorias,ganancias)==-1)
    {
        return false;
    }
    else
    {
        guardaListaCaballosDAT(x,nombre,DIE,victorias,ganancias);
        return true;
    }

}
