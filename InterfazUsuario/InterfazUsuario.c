//Contiene las funciones de inicializaci�n de la interfaz, as� como
//la gesti�n el men� principal y los dem�s men�s

//Funciones de biblioteca
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <locale.h>
#include <conio.h>
#include <stdbool.h>
#include "InterfazUsuario.h"
#include "../InterfazUsuario/interfazGrafica.h"
#include "../InterfazUsuario/caballoIU.h"



void inicInterfazUsuario()
{
    //INTERFAZ

    //CMD
    system("color 3F");//Color de fondo y fuente de la consola
    system("Title Proyecto Integrador"); //T�tulo del programa al ejecutarlo
    system("mode con cols=125 lines=30"); //�rea del programa

    char i;
    setlocale(LC_CTYPE,"C");

    //RECUADRO SUPERIOR

    gotoxy(0,124);//Esquina arriba izda.
    printf("%c",201);

    gotoxy(1,0);//L�neas dobles horizontales arriba
    for (i=2; i<=124; i++)
    {
        printf("%C",205);
    }

    gotoxy(0,1);//L�nea vertical doble izda.
    printf("%c",186);

    gotoxy(0,2);//Esquina doble abajo izda.
    printf("%c",200);

    gotoxy(124,0);//Esquina doble arriba dcha.
    printf("%c",187);

    gotoxy(1,2);//L�neas dobles horizontales abajo
    for (i=2; i<=124; i++)
    {
        printf("%C",205);
    }

    gotoxy(124,1);//L�nea vertical doble dcha.
    printf("%c",186);

    gotoxy(124,2);//Esquina doble abajo dcha.
    printf("%c",188);



    //RECUADRO CENTRAL IZDO.

    gotoxy(0,3);//Esquina superior izda.
    printf("%c",218);

    gotoxy(1,3);//L�nea horizontal simple inferior
    for (i=1; i<=60; i++)
    {
        printf("%C",196);
    }

    gotoxy(61,3);//Esquina superior derecha
    printf("%c",191);

    gotoxy(61,4);//L�nea vertical
    printf("%c",179);

    gotoxy(0,4);//L�nea vertical
    printf("%c",179);

    gotoxy(0,5);//Esquina 3 ramas hacia la dcha.
    printf("%c",195);

    gotoxy(0,6);//L�nea vertical
    printf("%c",179);

    gotoxy(1,5);//L�nea horizontal simple superior
    for (i=1; i<=61; i++)
    {
        printf("%C",196);
    }

    gotoxy(1,7);//L�nea horizontal simple inferior
    for (i=1; i<=61; i++)
    {
        printf("%C",196);
    }

    gotoxy(0,10);//L�nea vertical simple izda.
    for (i=0; i<=12; i++)
    {
        gotoxy(0,i+7);
        printf("%c",179);
    }

    gotoxy(61,5);//Esquina arriba dcha.
    printf("%c",180);

    gotoxy(61,6);//L�nea vertical dcha.
    printf("%c",179);

    gotoxy(61,7);//L�nea vertical simple dcha.
    for (i=1; i<=12; i++)
    {
        gotoxy(61,i+7);
        printf("%c",179);
    }

    gotoxy(61,7);//Esquina 3 ramas dcha.
    printf("%c",180);

    gotoxy(0,7);//Esquina 3 ramas izda.
    printf("%c",195);

    gotoxy(0,20);//Esquina abajo izda.
    printf("%c",192);

    gotoxy(0,7);//L�nea vertical simple izda.

    gotoxy(1,20);//L�nea simple abajo
    for (i=1; i<=60; i++)
    {
        printf("%c",196);
    }

    gotoxy(61,20);//Esquina abajo dcha.
    printf("%c",217);


    //RECUADRO DCHO.

    gotoxy(62,3);//Esquina superior izda.
    printf("%c",218);

    gotoxy(63,3);//L�nea horizontal simple inferior
    for (i=1; i<=61; i++)
    {
        printf("%C",196);
    }

    gotoxy(125,3);//Esquina superior derecha
    printf("%c",191);

    gotoxy(124,4);//Esquina superior derecha
    printf("%c",179);

    gotoxy(124,5);//Esquina 3 ramas hacia la izda.
    printf("%c",180);

    gotoxy(124,6);//L�nea vertical
    printf("%c",179);

    gotoxy(62,4);//L�nea vertical
    printf("%c",179);

    gotoxy(62,4);//L�nea vertical
    printf("%c",179);

    gotoxy(62,5);//Esquina 3 ramas hacia la dcha.
    printf("%c",195);

    gotoxy(62,6);//L�nea vertical
    printf("%c",179);

    gotoxy(63,5);//L�nea horizontal simple superior
    for (i=1; i<=61; i++)
    {
        printf("%C",196);
    }

    gotoxy(63,7);//L�nea horizontal simple inferior
    for (i=1; i<=61; i++)
    {
        printf("%c",196);
    }


    gotoxy(125,5);//Esquina 3 ramas dcha.
    printf("%c",180);

    gotoxy(125,6);//L�nea vertical dcha.
    printf("%c",179);

    gotoxy(124,7);//L�nea vertical simple dcha.
    for (i=1; i<=12; i++)
    {
        gotoxy(124,i+7);
        printf("%c",179);
    }

    gotoxy(61,7);//Esquina 3 ramas dcha.
    printf("%c",180);

    gotoxy(62,7);//Esquina 3 ramas izda.
    printf("%c",195);

    gotoxy(0,20);//Esquina abajo izda.
    printf("%c",192);

    gotoxy(62,8);//L�nea vertical izda.
    for(i=1; i<=12; i++)
    {
        gotoxy(62,i+7);
        printf("%c",179);
    }

    gotoxy(62,20);//Esquina abajo izda.
    printf("%c",192);

    gotoxy(63,20);//L�nea simple abajo
    for (i=1; i<=61; i++)
    {
        printf("%c",196);
    }

    gotoxy(124,20);//Esquina abajo dcha.
    printf("%c",217);



    //RECUADRO INFERIOR

    gotoxy(4,22);//L�nea superior horizontal
    for (i=5; i<=121; i++)
    {
        printf("%c",196);
    }

    gotoxy(4,25);//L�nea inferior horizontal
    for (i=5; i<=121; i++)
    {
        printf("%c",196);
    }

    gotoxy(3,22);//Esquina superior izda.
    printf("%c",218);

    gotoxy(3,25);//Esquina inferior izda.
    printf("%c",192);

    gotoxy(3,23);//L�nea vertical izda.
    printf("%c",179);

    gotoxy(3,24);//L�nea vertical izda.
    printf("%c",179);

    gotoxy(121,22);//Esquina superior dcha.
    printf("%c",191);

    gotoxy(121,25);//Esquina inferior dcha.
    printf("%c",217);

    gotoxy(121,23);//L�nea vertical dcha
    printf("%c",179);

    gotoxy(121,24);//L�nea vertical dcha.
    printf("%c",179);


    setlocale(LC_CTYPE,"Spanish");

    gotoxy(6,4);//Men� principal
    printf("          M E N �  P R I N C I P A L            ");


    gotoxy(2,17);//Borrar para el while de cambio de men�
    printf("                                           ");
    gotoxy(2,19);
    printf("                                           ");

    gotoxy(53,1);//T�tulo
    printf("H I P � D R O M O");
    return;

}


int menuPrincipal()//Muestra las opciones generales del proyecto Hip�dromo
{

    gotoxy(1,9);//Posici�n cursor
    printf("    1. Gesti�n de caballos\n");
    gotoxy(1,11);//Posici�n cursor
    printf("    2. Gesti�n de carreras\n");
    gotoxy(1,13);//Posici�n cursor
    printf("    3. Inicio de carrera\n");
    gotoxy(1,15);//Posici�n cursor
    printf("    0. Salir\n");

    return leerOpcionValida("Introduzca la opci�n deseada:",'3');//Devuelve la opci�n deseada sin presionar enter
}


void gestionMenuPrincipal()
{
    int opcion=9;//Valor aleatorio para que "opci�n" se inicie sin problemas

    while(opcion!=0)//Bucle while para poder adelantar y retroceder en el men�
    {
        gotoxy(6,4);//Men� principal
        printf("          M E N �  P R I N C I P A L            ");

        opcion=menuPrincipal();//Valor de la variable opci�n, en este caso es el men� principal
        switch(opcion)//Se utiliza un switch para poder elegir la zona del men� a la que se quiere acceder
        {

        case 1://Opci�n que se escoge al presionar 1
            gestionMenuCaballos();//Llamada a la funci�n que se elige al presionar 1
            break;

        case 2://Opci�n que se escoge al presionar 2
            gestionCarreras();//Llamada a la funci�n que se elige al presionar 2
            break;

        case 3://Opci�n que se escoge al presionar 3
            limpieza();
            gotoxy(10,4);
            printf("            C A R R E R A S             ");
            gotoxy(6,23);
            printf("FUNCI�N NO IMPLEMENTADA");
            getch();//El programa espera a que se introduzca un car�cter antes de pasar a la siguiente instrucci�n

            break;
        }
        limpieza();
    }
    return;
}


void gestionCarreras()
{
    gotoxy(1,9);
    printf("                                       ");
    gotoxy(10,4);
    printf(" G E S T I � N  D E  C A R R E R A S   ");
    gotoxy(1,11);
    printf("                                       ");
    gotoxy(1,13);
    printf("                                       ");
    gotoxy(1,15);
    printf("                                       ");
    gotoxy(6,23);
    printf("                                 ");
    gotoxy(6,23);
    printf("FUNCI�N NO IMPLEMENTADA");

    getch();//El programa espera a que se introduzca un car�cter antes de pasar a la siguiente instrucci�n
    return;
}
