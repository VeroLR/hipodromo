#ifndef CABALLOIU_H_INCLUDED
#define CABALLOIU_H_INCLUDED

void altaCaballoIU();//Agrega los datos de un nuevo caballo
void gestionMenuCaballos();//Men� de gesti�n de caballos
void listadoCaballos();//Muestra por pantalla los caballos guardados en los .txt
int menuCaballos();//Men� caballos
void muestraCaballo(char nombre[], long DIE, int victorias, float ganancias);//Muestra los datos de los caballos por pantalla
int bajaCaballoIU();//Funci�n que permite dar de baja a un caballo
bool ActualizaCaballoIU();//Funci�n que permite actualizar los datos de un caballo
int caballoMasRentableIU();//Funci�n que da el caballo con m�s gannacias
int caballoMasVictoriosoIU();//Funci�n que da el caballo con m�s victorias

#endif // CABALLO_H_INCLUDED
