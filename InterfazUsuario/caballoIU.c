//Contiene las funciones relacionadas con la interacci�n del usuario con los datos de caballos

//Funciones de biblioteca
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <stdbool.h>
#include <strings.h>
#include "../InterfazUsuario/caballoIU.h"
#include "../InterfazSistema/CaballoSys.h"
#include "InterfazGrafica.h"
#include "InterfazUsuario.h"


void altaCaballoIU()//Agrega los datos de un nuevo caballo.
{

    char nombre[200];
    long DIE;
    int victorias;
    float ganancias;

    limpieza();

    //Interfaz del men� altaCaballoIU
    gotoxy(10,4);
    printf("                                                  ");
    gotoxy(10,4);
    printf(" A L T A  D E  U N  N U E V O  C A B A L L O");
    gotoxy(2,9);
    printf("   Nombre:                                        ");
    gotoxy(2,11);
    printf("   DIE:                                           ");
    gotoxy(2,13);
    printf("   Victorias:                                     ");
    gotoxy(2,15);
    printf("   Ganancias:                                     ");
    gotoxy(1,17);
    printf("                                                  ");
    gotoxy(1,19);
    printf("                                                  ");

    //Introducci�n del nombre
    muestraMensajeInfo("Introduzca el nombre de su caballo:            ");
    gotoxy(6,24);
    fgets(nombre,sizeof(nombre),stdin);
    //fgets(nombre,sizeof(nombre),stdin);//Recoge por teclado una cadena de caracteres
    gotoxy(2,9);
    printf("   Nombre: %s",nombre);//Se muestra por pantalla una cadena de caracteres

    //Introducci�n del DIE
    muestraMensajeInfo("Introduzca el DIE de su caballo:                    ");
    gotoxy(6,24);
    printf("                                                    ");
    gotoxy(6,24);
    scanf("%ld",&DIE);//Recoge por teclado una cadena de caracteres

    while (DIE>999999||0>DIE)//Si el DIE tiene m�s de 6 d�gitos el usuario debe volver a introducir el n�mero, para ello se usa un bucle while
    {
        muestraMensajeInfo("El DIE no es v�lido. Introd�zcalo de nuevo.");//Muestra un mensaje por pantalla
        gotoxy(6,24);
        printf("                             ");
        gotoxy(6,24);
        scanf("%ld",&DIE);//Recoge por teclado una cadena de caracteres
    }

    gotoxy(2,11);
    printf("   DIE: %ld",DIE);//Se muestra por pantalla una cadena de caracteres

    //Introducci�n de las victorias
    muestraMensajeInfo("Introduzca las carreras ganadas por su caballo:");
    gotoxy(6,24);
    printf("                                                     ");
    gotoxy(6,24);
    scanf("%i",&victorias); //Recoge por teclado una cadena de caracteres
    gotoxy(2,13);
    printf("   Victorias: %i",victorias);//Se muestra por pantalla una cadena

    //Introducci�n de las ganancias
    muestraMensajeInfo("Introduzca las ganancias obtenidas con su caballo:");
    gotoxy(6,24);
    printf("                                     ");
    gotoxy(6,24);
    scanf("%f",&ganancias);//Recoge por teclado una cadena de caracteres
    gotoxy(2,15);
    printf("   Ganancias: %f",ganancias);//Se muestra por pantalla una cadena

    altaCaballoSys(nombre,DIE,victorias,ganancias);//Se env�an los datos recogidos en la funci�n altaCaballoIU a altaCaballoSys
    limpieza();


    return;

}

void gestionMenuCaballos()//Gestiona la lista de caballos del hip�dromo.
{
    int m=9;//Valor que se da a 'm' para que se inicie correctamente la variable int

    while(m!=0)//Se usa un while para poder crear un men� que permita avanzar y retroceder
    {
        m=menuCaballos();
        switch(m)//Se usa un switch para elegir entre las diferentes opciones que presenta el men�
        {
        case 1://Si se pulsa '1'
            listadoCaballos();//Se llama a la funci�n listadoCaballos
            break;
        case 2://Si se pulsa '2'
            generaInformeCaballos();//Se llama a la funci�n generaInformeCaballos
            break;
        case 3://Si se pulsa '3'
            altaCaballoIU();//Se llama a la funci�n altaCaballoIU
            break;
        case 4://Si se pulsa '4'
            ActualizaCaballoIU();//Se llama a la funci�n ActualizaCaballoIU
            break;
        case 5://Si se pulsa '5'
            bajaCaballoIU();//Se llama a la funci�n bajaCaballoIU
            break;
        case 6://Si se pulsa '6'
            caballoMasRentableIU();//Se llama a la funci�n caballoMasRentableIU
            break;
        case 7://Si se pulsa '7'
            caballoMasVictoriosoIU();//Se llama a la funci�n caballoMasVictoriosoIU
            break;
        }
        //limpieza();
    }
    return;
}


void listadoCaballos()//Funci�n que muestra por pantalla la lista de caballos guardados en caballos.txt
{
    limpieza();//Llamada a funci�n para limpiar la consola
    char nombre[200][30],s;//Variable que guarda los nombres los caballos en una matriz bidimiensional de tama�o 200x30 (200 nombres como m�ximo de 30 caracteres cada uno como m�ximo)
    long DIE[200];//Variable que guarda los DIE's en un vector de tama�o 200
    int victorias[200];//Variable que guarda las victorias en un vector de tama�o 200
    float ganancias[200];//Variable que guarda las ganancias en un vector de tama�o 200
    int i,numero_caballos;//Variable del contador

    gotoxy(10,4);
    printf(" L I S T A D O  D E  C A B A L L O S"   );

    numero_caballos=cargaListaCaballosSys(nombre,DIE,victorias,ganancias,"Mensaje");//Se llama a esta funci�n para recoger los datos de los caballos guardados

    gotoxy(4,6);
    printf("DIE");
    gotoxy(12,6);
    printf("Victorias");
    gotoxy(24,6);
    printf("Ganancias");
    gotoxy(40,6);
    printf("Nombre");

    gotoxy(68,6);
    printf("DIE");
    gotoxy(76,6);
    printf("Victorias");
    gotoxy(88,6);
    printf("Ganancias");
    gotoxy(104,6);
    printf("Nombre");


    for(i=0; i<numero_caballos; i++) //Se usa un if con un contador que permite mostrar los 10 primeros caballos guardados
    {
        if(i<=11)//Cuando el n�mero de caballos es menor que 11 aparecen en el cuadro izquierdo
        {
            gotoxy(4,8+i);//Posici�n del cursor
            muestraCaballo(nombre[i],DIE[i],victorias[i],ganancias[i]);//Llamada a la funci�n para que muestre los datos de los caballos
        }
    }
    for(i=12; i<numero_caballos; i++)
    {
        if(i<=23)//Cuando el n�mero de caballos es superior a 11 aparecen en el cuadro derecho
        {
            gotoxy(68,-4+i);//Posici�n del cursor
            muestraCaballo(nombre[i],DIE[i],victorias[i],ganancias[i]);//Llamada a la funci�n para que muestre los datos de los caballos

        }
    }

    gotoxy(6,23);
    muestraMensajeInfo("Pulse 's' para pasar de p�gina. <Enter> para permanecer en esta.");
    s=getche();

    if(s=='s')
    {

        limpieza();

        gotoxy(10,4);
        printf(" L I S T A D O  D E  C A B A L L O S"   );

        gotoxy(4,6);
        printf("DIE");
        gotoxy(12,6);
        printf("Victorias");
        gotoxy(24,6);
        printf("Ganancias");
        gotoxy(40,6);
        printf("Nombre");

        gotoxy(68,6);
        printf("DIE");
        gotoxy(76,6);
        printf("Victorias");
        gotoxy(88,6);
        printf("Ganancias");
        gotoxy(104,6);
        printf("Nombre");

        for(i=24; i<numero_caballos; i++) //Se usa un if con un contador que permite mostrar los 10 primeros caballos guardados
        {
            if(i<=34)//Cuando el n�mero de caballos es menor que 11 aparecen en el cuadro izquierdo
            {
                gotoxy(4,-16+i);//Posici�n del cursor
                muestraCaballo(nombre[i],DIE[i],victorias[i],ganancias[i]);//Llamada a la funci�n para que muestre los datos de los caballos
            }

        }
        for(i=36; i<numero_caballos; i++)
        {
            if(i>=34)//Cuando el n�mero de caballos es superior a 11 aparecen en el cuadro derecho
            {
                gotoxy(68,-4+i);//Posici�n del cursor
                muestraCaballo(nombre[i],DIE[i],victorias[i],ganancias[i]);//Llamada a la funci�n para que muestre los datos de los caballos
            }
        }

    }
    if(s=='a')//Si se pulsa 'a', se vuelve al men� principal
    {
        return;
    }

    gotoxy(6,23);
    muestraMensajeInfo("Presione una tecla para continuar... ");
    getch();//El programa espera a que el usuario introduzca un car�cter antes de pasar a la siguiente instrucci�n
    //limpieza();
    return;

}


void muestraCaballo(char nombre[], long DIE, int victorias, float ganancias)//Funci�n que muestra los datos de los caballos guardados por pantalla
{
    printf("%ld\t%d\t%f\t%s\n",DIE,victorias,ganancias,nombre);
}

int menuCaballos()//Muestra las opciones relacionadas con los caballos.
{
    limpieza();
    gotoxy(10,4);
    printf("G E S T I � N  D E  C A B A L L O S");

    gotoxy(2,9);
    printf("   1. Listado de caballos\n");
    gotoxy(2,10);
    printf("   2. Informe de caballos\n");
    gotoxy(2,11);
    printf("   3. Tramitar alta de un caballo\n");
    gotoxy(2,12);
    printf("   4. Actualizar caballo\n");
    gotoxy(2,13);
    printf("   5. Tramitar baja de un caballo\n");
    gotoxy(2,14);
    printf("   6. Caballo m�s rentable");
    gotoxy(2,15);
    printf("   7. Caballo m�s victorioso");
    gotoxy(2,16);
    printf("   0. Men� anterior");

    return leerOpcionValida("Introduzca la opci�n deseada:",'7');//Espera a que el usuario introduzca una de las opciones del men�
}

int bajaCaballoIU()//Funci�n a�n no implementada en el proyecto
{
    char nombre[200][30],s;
    long DIE[200],die;
    int victorias[200],x,a=-1,i;
    float ganancias[200];

    x=cargaListaCaballosSys(nombre,DIE,victorias,ganancias,"Mensaje");//Llama a la funci�n para cargar los datos de los caballos en un conjunto de vectores paralelos
    listadoCaballos();//Llama a la funci�n listado de caballos para que aparezcan por pantalla

    gotoxy(6,23);
    printf("                                    ");//Borra el texto "Listado de caballos"
    gotoxy(10,4);
    printf("B A J A  D E  U N  C A B A L L O    ");//T�tulo del men�
    muestraMensajeInfo("Introduzca el DIE del caballo que desea dar de baja:");//Se introduce el DIE del caballo que se quiere dar de baja
    gotoxy(58,23);
    scanf("%ld",&die);//Se introduce por pantalla el DIE y se almacena en &ld

    if(x==-1)//Si cargaListaCaballosSys no carga los datos correctamente se ejecuta lo siguiente
    {
        muestraMensajeInfo("No se ha podido cargar correctamente los datos de los caballos\a");
        return -1;//Finaliza el programa
    }
    for (i=0; i<x; i++)//Mientras i sea menor que el n�mero de datos de cargaListaCaballosSys, i se incrementa
    {
        if(die==DIE[i])//Si el DIE introducido se encuentra en la misma posici�n de los vectores paralelos que el otro DIE
        {
            a=i;//entonces a=i
        }

    }
    if (a==-1)
    {
        muestraMensajeInfo("No se ha podido encontrar el caballo. Pulse cualquier tecla para continuar.");
        getch();
        return 0;
    }
    gotoxy(6,23);
    printf("                                                                            ");
    nombre[a][strlen(nombre[a])-1]='\0';//Dado q
    gotoxy(6,23);
    printf("�Desea dar de baja al caballo %s\?",nombre[a]);//Se debe elegir entre s� y no, %s muestra el caballo guardado en la posici�n a=i

    fflush(stdin);//Borra la memoria temporal o buffer
    s=getche();//El programa espera que la tecla que se introduzca sea 's'
    if(s=='s')//Si se introduce 's', el programa ejecuta la siguiente instrucci�n
    {
        for(; a<(x-1); a++) //Cuando la posici�n a del caballo sea el �ltimo caballo guardado en cargaListaCaballosSys, a se incrementa y se ejecuta la siguiente instrucci�n
        {
            strcpy(nombre[a],nombre[a+1]);//Se copia el �ltimo nombre en el pen�ltimo y se sobreescribe
            DIE[a]=DIE[a+1];//Se copia el �ltimo DIE en el pen�ltimo y se sobreescribe
            victorias[a]=victorias[a+1];//Se copian las �ltimas victorias en las victorias y se sobreescriben
            ganancias[a]=ganancias[a+1];//Se copian las �ltimas ganancias en las pen�ltimas y se sobreescriben
        }

        if(guardaListaCaballosSys(x,nombre,DIE,victorias,ganancias,"Mensaje")==-1)//Si guardaListaCaballosSys no carga los datos correctamente
        {
            return -1;//El programa finaliza
        }
        else//En caso contrario
        {
            listadoCaballos();//Se llama a la funci�n listadoCaballos y muestra la lista con todos los caballos guardados
            muestraMensajeInfo("El caballo se ha dado de baja con �xito. Pulse una tecla para continuar");
            getch();//El programa espera a que el usuario introduzca una tecla cualquiera
        }
    }

    else if ('n'==s)//En caso de que la tecla introducida sea n
    {
        return 0;//El programa retrocede al men� anterior
    }

    return 0;
}

bool ActualizaCaballoIU()//Funci�n que permite dar de baja a un caballo
{
    char nombre[200][30],nombre2[200],s=1;
    float ganancias[200];
    long DIE[200];
    int x,i=0,a=-1,victorias[200];


    limpieza();//Limpia la consola para que se pueda sobreescribir
    listadoCaballos();//Llama a la funci�n listadoCaballos para que aparezcan por pantalla todos los caballos registrados

    if(s=='a')//Permite volver al men� anterior
    {
        return false;
    }


    gotoxy(6,4);
    printf("A C T U A L I Z A C I � N  D E  U N  C A B A L L O");
    muestraMensajeInfo("Introduzca el nombre del caballo ganador:");//Muestra un mensaje por pantalla
    gotoxy(6,24);
    fgets(nombre2,sizeof(nombre2),stdin);//Recoge por teclado una cadena de caracteres
    gotoxy(6,23);
    printf("                                                  ");

    x=cargaListaCaballosSys(nombre,DIE,victorias,ganancias,"Mensaje");//Llama a la funci�n cargaListaCaballosSys para que se carguen los
    //datos de los caballos registrados

    if (x==-1)//En caso de que no se carguen correctamente los datos de los caballos se ejecuta la siguiente instrucci�n
    {
        muestraMensajeInfo("No se ha podido cargar la lista de caballos.");//Muestra un aviso por pantalla
        return false;//Devuelve false y el programa finaliza
    }

    for (i=0; i<x; i++)//Se ejecuta un bucle for desde la posici�n i=0 y mientras que i sea menor que la posici�n que se est� leyendo en cargaListaCaballosSys
        //el contador i aumenta
    {
        if(strcmp(nombre2,nombre[i])==0)//Se comparan dos cadenas de caracteres y en caso de que ambas sean iguales se ejecuta lo siguiente
        {
            a=i;//Se almacena la posici�n recogida i en la posici�n a
        }
    }
    if(a==-1)
    {
        muestraMensajeInfo("No se ha podido encontrar el caballo introducido. Pulse cualquier tecla para continuar.");
        getch();
        return 0;
    }
    muestraMensajeInfo("Introduzca las ganancias obtenidas:");//Muestra un mensaje por pantalla
    gotoxy(6,24);
    printf("                          ");
    gotoxy(6,24);
    scanf("%f",&ganancias[a]);//Recoge unas ganancias que se almacenar�n en la posici�n a que coincide con la posici�n i recogida en el anterior if

    if(guardaListaCaballosSys(x,nombre,DIE,victorias,ganancias,"Mensaje")==-1)//Si guardaListaCaballosSys no carga los datos correctamente
    {
        return -1;//El programa finaliza
    }

    else//En caso contrario
    {
        listadoCaballos();//Se llama a la funci�n listadoCaballos y muestra la lista con todos los caballos guardados
        gotoxy(6,4);
        printf("A C T U A L I Z A C I � N  D E  U N  C A B A L L O");
        muestraMensajeInfo("El caballo se ha actualizado con �xito. Pulse una tecla para continuar");//Muestra un mensaje por pantalla
        getch();//El programa espera a que el usuario introduzca una tecla cualquiera
    }
    return 0;

}

int caballoMasRentableIU()
{
    char nombre[200][30],msg[]= {"El caballo con m�s ganancias es: "};
    long DIE[200];
    int victorias[200];
    float ganancias[200];
    int x,a,i=0,gan=-1;

    x=cargaListaCaballosSys(nombre,DIE,victorias,ganancias,"Error");//Se llama a la funci�n cargaListaCaballosSys para cargar los datos de los caballos registrados

    if(x==-1)//Si cargaListaCaballosSys no carga correctamente los datos de los caballos el programa ejecuta la siguiente instrucci�n
    {
        return -1;//El programa finaliza
    }

    for (i=0; i<x; i++)//En caso de que los datos se carguen correctamente, se ejecuta el bucle for comenzando en la posici�n i=0.
        //Mientras que la posici�n de i sea menor que la posici�n de los vectores en cargaListaCaballosSys el contador i aumenta.
    {
        if(gan<ganancias[i])//Si la variable gan es menor que la posici�n i del vector de ganancias de cargaListaCaballosSys se ejecuta lo siguiente
        {
            a=i;//Se guarda la posici�n i en la variable a
            gan=ganancias[i];
        }

    }

    limpieza();
    gotoxy(9,4);
    printf("C A B A L L O  M � S  R E N T A B L E");
    gotoxy(4,6);
    printf("DIE");
    gotoxy(13,6);
    printf("Victorias");
    gotoxy(26,6);
    printf("Ganancias");
    gotoxy(40,6);
    printf("Nombre");
    gotoxy(4,8);
    muestraCaballo(nombre[a],DIE[a],victorias[a],ganancias[a]);//El caballo en la posici�n a=i se muestra por pantalla
    strcat(msg,nombre[a]);//Permite realizar una concatenaci�n de strings
    muestraMensajeInfo(msg);//Muestra un mensaje por pantalla
    gotoxy(6,24);
    printf("Pulse cualquier tecla para continuar.");
    getch();//El programa espera a que el usuario introduzca una tecla


    return 0;
}


int caballoMasVictoriosoIU()
{
    char nombre[200][30],msg[]= {"El caballo con m�s victorias es: "};
    long DIE[200];
    int victorias[200];
    float ganancias[200];
    int x,a,i=0,vict=-1;

    x=cargaListaCaballosSys(nombre,DIE,victorias,ganancias,"Error"); //Se llama a la funci�n cargaListaCaballosSys para cargar los datos de los caballos registrados

    if(x==-1)//Si cargaListaCaballosSys no carga correctamente los datos de los caballos el programa ejecuta la siguiente instrucci�n
    {
        return -1;//El programa finaliza
    }

    for (i=0; i<x; i++)//En caso de que los datos se carguen correctamente, se ejecuta el bucle for comenzando en la posici�n i=0.
        //Mientras que la posici�n de i sea menor que la posici�n de los vectores en cargaListaCaballosSys el contador i aumenta.
    {
        if(vict<victorias[i])//Si la variable victo es menor que la posici�n i del vector de victorias de cargaListaCaballosSys se ejecuta lo siguiente
        {
            a=i;//Se guarda la posici�n i en la variable a
            vict=victorias[i];
        }

    }

    limpieza();
    gotoxy(8,4);
    printf("C A B A L L O  M � S  V I C T O R I O S O");
    gotoxy(4,6);
    printf("DIE");
    gotoxy(13,6);
    printf("Victorias");
    gotoxy(26,6);
    printf("Ganancias");
    gotoxy(40,6);
    printf("Nombre");
    gotoxy(4,8);
    muestraCaballo(nombre[a],DIE[a],victorias[a],ganancias[a]);//El caballo en la posici�n a=i se muestra por pantalla
    strcat(msg,nombre[a]);;//Permite realizar una concatenaci�n de strings
    muestraMensajeInfo(msg);//Muestra un mensaje por pantalla
    gotoxy(6,24);
    printf("Pulse cualquier tecla para continuar.");
    getch();//El programa espera a que el usuario introduzca una tecla
    limpieza();

    return 0;
}
