//Interfaz gr�fica del proyecto Hip�dromo

//Funciones de biblioteca
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <locale.h>
#include <conio.h>
#include <stdbool.h>
#include "interfazGrafica.h"


void gotoxy (int x, int y) //Funci�n declarada de la situaci�n del cursor en la pantalla
{
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);

    return;
}

void setColorTexto (WORD colors)//Colores utilizados en la interfaz
{
    HANDLE hConsole=GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, colors);
    return;
}

void muestraMensajeInfo(char *msg)//Muestra un mensaje en la ventana de mensajes.
{
    gotoxy(6,23);
    printf("                                                                                                          ");
    gotoxy(6,23);
    printf("%s",msg);
    return;
}


int leerOpcionValida(char *mensaje,char num)//Permite leer la opci�n que introduce el usuario sin pulsar enter
{
    char opcion;//Variable en la que se guarda el valor que se introduce por teclado
    muestraMensajeInfo(mensaje);//Muestra un mensaje en la ventana de mensajes
    opcion=num+2;//Se asegura que la funci�n entre en el bucle
    while(48>opcion||opcion>num)
    {
        opcion=getch();//Conversi�n ASCII-decimal
    }
    return opcion-48;
}

void limpieza()//Funci�n para limpiar pantalla
{
    //Limpia el cuadro izquierdo
    gotoxy(4,6);
    printf("                                                        ");
    gotoxy(7,4);
    printf("                                                     ");
    gotoxy(6,4);
    printf("                                                     ");
    gotoxy(10,4);
    printf("                                                 ");
    gotoxy(1,8);
    printf("                                                         ");
    gotoxy(1,9);
    printf("                                                         ");
    gotoxy(1,10);
    printf("                                                         ");
    gotoxy(1,11);
    printf("                                                         ");
    gotoxy(1,12);
    printf("                                                         ");
    gotoxy(1,13);
    printf("                                                         ");
    gotoxy(1,14);
    printf("                                                         ");
    gotoxy(1,15);
    printf("                                                         ");
    gotoxy(1,16);
    printf("                                                         ");
    gotoxy(1,17);
    printf("                                                         ");
    gotoxy(1,18);
    printf("                                                         ");
    gotoxy(1,19);
    printf("                                                         ");
    gotoxy(6,23);
    printf("                                                                                           ");
    gotoxy(6,24);
    printf("                                       ");

    //Limpia el uadro derecho
    gotoxy(68,6);
    printf("           ");
    gotoxy(76,6);
    printf("           ");
    gotoxy(88,6);
    printf("           ");
    gotoxy(104,6);
    printf("           ");
    gotoxy(68,8);
    printf("                                              ");
    gotoxy(68,9);
    printf("                                              ");
    gotoxy(68,10);
    printf("                                              ");
    gotoxy(68,11);
    printf("                                              ");
    gotoxy(68,12);
    printf("                                              ");
    gotoxy(68,13);
    printf("                                              ");
    gotoxy(68,14);
    printf("                                              ");
    gotoxy(68,15);
    printf("                                              ");
    gotoxy(68,16);
    printf("                                              ");
    gotoxy(68,17);
    printf("                                              ");
    gotoxy(68,18);
    printf("                                              ");
    gotoxy(68,19);
    printf("                                              ");

}
