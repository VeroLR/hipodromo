#ifndef INTERFAZGRAFICA_H_INCLUDED
#define INTERFAZGRAFICA_H_INCLUDED

void setColorTexto(WORD colors);
void muestraMensajeInfo (char *msg);//Muestra un mensaje en la ventana de mensajes
void gotoxy (int x, int y); //Sit�a el cursor en un sitio deseado de la pantalla
int leerOpcionValida(char *mensaje,char num);//Elegir opci�n sin enter

//Esta funci�n es propia
void limpieza();//Limpia la interfaz

#endif // INTERFAZGRAFICA_H_INCLUDED
